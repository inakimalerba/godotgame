extends KinematicBody2D

const speed_slow = 80 # pixels per second
const speed_fast = 120 # pixels per second
const shoot_ratio_slow = 10
const shoot_ratio_fast = 80

var name = "Unnamed"
var controlled = false
var anim = "Idle"
var points = []
var pos = Vector2(0,0)
#variables del inventario personal
var vida = 100
var energia = 0
var velocidad = speed_slow
var velocidad_disparo = shoot_ratio_slow
var botiquines = 3




func _ready():
	get_node("Camera2D").set_zoom(get_node("Camera2D").get_zoom() * get_node("/root/global").viewport_scale) # Setear zoom adaptable a pantalla
	add_to_group("Players")
	get_node("Nametag").set_text(name)
	set_fixed_process(true)

func MakePlayer():
	controlled = true
	get_node("Camera2D").make_current()

func _fixed_process( delta ):
	var motion = Vector2()
	
	var up = Input.is_action_pressed("btn_u")
	var down = Input.is_action_pressed("btn_d")
	var left = Input.is_action_pressed("btn_l")
	var right = Input.is_action_pressed("btn_r")
	
	var shooting = Input.is_action_pressed("esacio")
	
	if shooting:
		var shot = preload("res://res/player/disparo/shot.scn").instance()
		#use the position3d as reference
		if down:
			shot.set_pos( get_parent().get_node("Player/disparo_abajo").get_global_pos() )
			#put it two parents above, so it is not moved by us
			get_node("../..").add_child(shot)
		else:
			shot.set_pos( get_parent().get_node("Player/disparo_arriba").get_global_pos() )
			#put it two parents above, so it is not moved by us
			get_node("../..").add_child(shot)
		print("SHOOT")
	
	
	#Probando
	if (Input.is_action_pressed("btn_u")):
		motion+=Vector2(0,-1)
	if (Input.is_action_pressed("btn_d")):
		motion+=Vector2(0,1)
	if (Input.is_action_pressed("btn_l")):
		motion+=Vector2(-1,0)
	if (Input.is_action_pressed("btn_r")):
		motion+=Vector2(1,0)
		
	move(motion*velocidad*delta)
	#Hasta acá
	
	
	
	
	if controlled:
		anim = "Idle"
		var motion = Vector2(0,0)
		if (Input.is_action_pressed("btn_u")):
			motion+=Vector2(0,-1)
		if (Input.is_action_pressed("btn_d")):
			motion+=Vector2(0,1)
		if (Input.is_action_pressed("btn_l")):
			motion+=Vector2(-1,0)
		if (Input.is_action_pressed("btn_r")):
			motion+=Vector2(1,0)
		
		#motion = motion.normalized() * speed * delta
		#motion = move(motion)
		move(motion*velocidad*delta)
		
		var slide_attempts = 4
		while(is_colliding() and slide_attempts>0):
			motion = get_collision_normal().slide(motion)
			motion=move(motion)
			slide_attempts-=1
			
		#original
		#if Input.is_action_pressed("ui_left"):
		#	anim = "Left"
		#	motion = Vector2(-1,0)
		#if Input.is_action_pressed("ui_right"):
		#	anim = "Right"
		#	motion = Vector2(1,0)
		#if Input.is_action_pressed("ui_down"):
		#	anim = "Down"
		#	motion = Vector2(0,1)
		#if Input.is_action_pressed("ui_up"):
		#	anim = "Up"
		#	motion = Vector2(0,-1)
		#
		#move(motion*speed*delta)
		
	if get_node("Animation").get_current_animation() != anim:
		get_node("Animation").play(anim)

func Quit():
	queue_free()
