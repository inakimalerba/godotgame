extends "res://net_constants.gd"

#
# ACA HACE LA CONECCION . ESTE SCRIPT SE EJECUTA AUTOMATICAMENTE Y SE CONECTA CON EL SERVER
# LA IP ESTA HARCODEADA.
# ESTO ANDA TODO BIEN, EL SERVIDOR SE ENTERA QUE SE CONECTA, LO MUESTRA Y TODO
# EL TEMA ESTA EN CUANDO TIENE QUE HACER ALGO.
# GOTO: global.gd
#


const port = 3561
var ip = "127.0.0.1"

var playerName = "playerName"

var connection # your connection (StreamPeerTCP) object
var peerstream # your data transfer (PacketPeerStream) object
var connected = false # for additional connection check
var peer


func _ready():
	
	connection = StreamPeerTCP.new()
	
	connection.connect( ip, port )
	peer = PacketPeerStream.new()
	peer.set_stream_peer( connection )
	
	# since connection is created from StreamPeerTCP it also inherits its constants
	# get_status() returns following (int 0-3) values:
	if connection.get_status() == connection.STATUS_CONNECTED:
		print( "Connected to "+ip+" :"+str(port) )
		set_process(true) # start processing if connected
		connected = true # finally you can use this var ;)
		peer.put_var( [ PLAYER_CONNECT, playerName ] ) # send our name to server
		
	elif connection.get_status() == StreamPeerTCP.STATUS_CONNECTING:
		print( "Trying to connect "+ip+" :"+str(port) )
		set_process(true) # or if trying to connect
	elif connection.get_status() == connection.STATUS_NONE or connection.get_status() == StreamPeerTCP.STATUS_ERROR:
		print( "Couldn't connect to "+ip+" :"+str(port) )

func _process( delta ):
	if !connected: # it's inside _process, so if last status was STATUS_CONNECTING
		if connection.get_status() == connection.STATUS_CONNECTED:
			print( "Connected to "+ip+" :"+str(port) )
			connected = true
			peer.put_var( [ PLAYER_CONNECT, playerName ] ) # send our name to server
			
			return # skipping this _process run
			
	if connection.get_status() == connection.STATUS_NONE or connection.get_status() == connection.STATUS_ERROR:
		print( "Server disconnected? " )
		set_process( false )






func _on_Button_Back_pressed():
	if connection:
		connection.disconnect()
	get_node("../Menu").show()
	queue_free() # remove yourself at idle frame
