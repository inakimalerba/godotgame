import socket
import sys
import threading

PLAYER_CONNECT = 0
PLAYER_DISCONNECT = 1
PLAYER_DATA = 2
NAME_TAKEN = 3
MESSAGE = 4

class client_data(threading.Thread):

    def __init__(self, player, ip, port, socket):
        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.socket = socket
        print "[+] New thread started for "+ip+":"+str(port)
    def __run__(self):
        print "__run__" # use self.socket to send/receive
    def start(self):

        connection, client_address = self.peer
        
        try:
           print >>sys.stderr, 'connection from', client_address

           # Receive the data in small chunks and retransmit it
           while True:
               data = connection.recv(16)
               #print >>sys.stderr, 'received "%s"' % data
               if data:
                   #print >>sys.stderr, 'sending data back to the client'
                   #connection.sendall(data)
                   pass
               else:
                   print >>sys.stderr, 'no more data from', client_address
                   break
        finally:
                pass
    def get_peer(self):
        return self.peer

connections = []

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = ('127.0.0.1', 3561)
print >> sys.stderr, 'starting up on %s port %s' % server_address
sock.bind(server_address)

sock.listen(1)

while True:
    # Wait for a connection
    #client = client_data(1, sock.accept())
    #client.start()
    (clientsock, (ip, port)) = sock.accept()
    newthread = client_data(1,ip, port, clientsock)
    print >>sys.stderr, 'waiting for a connection'
    #connection, client_address = client.get_peer()
    #try:
    #    print >>sys.stderr, 'connection from', client_address

    #    # Receive the data in small chunks and retransmit it
    #    '''while True:
    #        data = connection.recv(16)
    #        #print >>sys.stderr, 'received "%s"' % data
    #        if data:
    #            #print >>sys.stderr, 'sending data back to the client'
    #            #connection.sendall(data)
    #            pass
    #        else:
    #            print >>sys.stderr, 'no more data from', client_address
    #            break'''
    #    while True:
    #        data = connection.recv(16)
    #        if data:
    #            pass
    #        else:
    #            print "No more data from "+str(client_address)
    #            
    #finally:
        # Clean up the connection
        #connection.close()
